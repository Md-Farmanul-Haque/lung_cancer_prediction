import pandas as pd
from sklearn.preprocessing import LabelEncoder, StandardScaler

def load_data(filepath: str) -> pd.DataFrame:
    """
    Load the dataset from a CSV file.
    
    Parameters:
    filepath (str): The path to the CSV file.
    
    Returns:
    pd.DataFrame: The loaded dataset.
    """
    return pd.read_csv(filepath)

def handle_missing_values(df: pd.DataFrame) -> pd.DataFrame:
    """
    Handle missing values in the dataset.
    
    Parameters:
    df (pd.DataFrame): The dataset with potential missing values.
    
    Returns:
    pd.DataFrame: The dataset with missing values handled.
    """
    return df.dropna()

def encode_categorical_variables(df: pd.DataFrame, columns: list) -> (pd.DataFrame, dict):
    """
    Encode categorical variables using Label Encoding.
    
    Parameters:
    df (pd.DataFrame): The dataset containing categorical variables.
    columns (list): List of columns to be encoded.
    
    Returns:
    (pd.DataFrame, dict): The dataset with encoded categorical variables and a dictionary of label encoders.
    """
    label_encoders = {}
    for column in columns:
        le = LabelEncoder()
        df[column] = le.fit_transform(df[column])
        label_encoders[column] = le
    return df, label_encoders

def scale_numerical_features(df: pd.DataFrame, columns: list) -> (pd.DataFrame, StandardScaler):
    """
    Scale numerical features using Standard Scaler.
    
    Parameters:
    df (pd.DataFrame): The dataset containing numerical features.
    columns (list): List of columns to be scaled.
    
    Returns:
    (pd.DataFrame, StandardScaler): The dataset with scaled numerical features and the scaler object.
    """
    scaler = StandardScaler()
    df[columns] = scaler.fit_transform(df[columns])
    return df, scaler

def save_processed_data(df: pd.DataFrame, filepath: str) -> None:
    """
    Save the processed dataset to a CSV file.
    
    Parameters:
    df (pd.DataFrame): The processed dataset.
    filepath (str): The path to save the processed CSV file.
    """
    df.to_csv(filepath, index=False)

if __name__ == "__main__":
    # Load data
    data = load_data('../data/raw/lung_cancer_data.csv')

    # Handle missing values
    data = handle_missing_values(data)

    # Columns to be encoded
    categorical_columns = ['GENDER', 'SMOKING', 'YELLOW_FINGERS', 'ANXIETY',
                           'PEER_PRESSURE', 'CHRONIC DISEASE', 'FATIGUE ',
                           'ALLERGY ', 'WHEEZING', 'ALCOHOL CONSUMING', 
                           'COUGHING', 'SHORTNESS OF BREATH', 
                           'SWALLOWING DIFFICULTY', 'CHEST PAIN', 'LUNG_CANCER']

    # Encode categorical variables
    data, encoders = encode_categorical_variables(data, categorical_columns)

    # Columns to be scaled (only AGE in this case)
    numerical_columns = ['AGE']

    # Scale numerical features
    data, scaler = scale_numerical_features(data, numerical_columns)

    # Save the processed data
    save_processed_data(data, '../data/processed/lung_cancer_processed.csv')
