import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

def load_data(filepath: str) -> pd.DataFrame:
    """
    Load the dataset from a CSV file.
    
    Parameters:
    filepath (str): The path to the CSV file.
    
    Returns:
    pd.DataFrame: The loaded dataset.
    """
    return pd.read_csv(filepath)

def plot_class_distribution(df: pd.DataFrame, column: str) -> None:
    """
    Plot the distribution of the target variable.
    
    Parameters:
    df (pd.DataFrame): The dataset.
    column (str): The column to plot the distribution of.
    """
    plt.figure(figsize=(8, 6))
    sns.countplot(x=column, data=df)
    plt.title('Class Distribution')
    plt.xlabel(column)
    plt.ylabel('Count')
    plt.show()

def plot_correlation_matrix(df: pd.DataFrame) -> None:
    """
    Plot the correlation matrix of the dataset.
    
    Parameters:
    df (pd.DataFrame): The dataset.
    """
    plt.figure(figsize=(12, 10))
    correlation_matrix = df.corr()
    sns.heatmap(correlation_matrix, annot=True, fmt='.2f', cmap='coolwarm')
    plt.title('Correlation Matrix')
    plt.show()

def plot_histograms(df: pd.DataFrame) -> None:
    """
    Plot histograms for all numerical features.
    
    Parameters:
    df (pd.DataFrame): The dataset.
    """
    df.hist(bins=30, figsize=(20, 15))
    plt.suptitle('Histograms of Numerical Features')
    plt.show()

def plot_pairplot(df: pd.DataFrame, target: str) -> None:
    """
    Plot pairplots to show relationships between features.
    
    Parameters:
    df (pd.DataFrame): The dataset.
    target (str): The target column for hue.
    """
    plt.figure(figsize=(12, 10))
    sns.pairplot(df, hue=target)
    plt.title('Pairplot of Features')
    plt.show()

if __name__ == "__main__":
    # Load data
    data = load_data('../data/processed/lung_cancer_processed.csv')

    # Plot class distribution
    plot_class_distribution(data, 'LUNG_CANCER')

    # Plot correlation matrix
    plot_correlation_matrix(data)

    # Plot histograms
    plot_histograms(data)

    # Plot pairplot
    plot_pairplot(data, 'LUNG_CANCER')
