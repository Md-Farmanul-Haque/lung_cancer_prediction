from sklearn.metrics import accuracy_score, classification_report, confusion_matrix

def evaluate_model(model, X_test, y_test, output_file):
    """
    Evaluate the performance of the model and store results in a file.
    
    Parameters:
    model: The trained model.
    X_test (pd.DataFrame): The features of the test set.
    y_test (pd.Series): The target variable of the test set.
    output_file (str): The path to the output file to store the evaluation results.
    """
    with open(output_file, 'w') as f:
        y_pred = model.predict(X_test)
        accuracy = accuracy_score(y_test, y_pred)
        f.write("Accuracy Score: {}\n".format(accuracy))
        f.write("Classification Report:\n")
        f.write(classification_report(y_test, y_pred) + "\n")
        f.write("Confusion Matrix:\n")
        f.write(str(confusion_matrix(y_test, y_pred)) + "\n")
