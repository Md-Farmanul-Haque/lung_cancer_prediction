import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix

def load_data(filepath: str) -> pd.DataFrame:
    """
    Load the dataset from a CSV file.
    
    Parameters:
    filepath (str): The path to the CSV file.
    
    Returns:
    pd.DataFrame: The loaded dataset.
    """
    return pd.read_csv(filepath)

def split_data(df: pd.DataFrame, target: str, test_size: float) -> (pd.DataFrame, pd.DataFrame, pd.Series, pd.Series):
    """
    Split the dataset into train and test sets.
    
    Parameters:
    df (pd.DataFrame): The dataset.
    target (str): The target column.
    test_size (float): The proportion of the dataset to include in the test split.
    
    Returns:
    (pd.DataFrame, pd.DataFrame, pd.Series, pd.Series): X_train, X_test, y_train, y_test
    """
    X = df.drop(columns=[target])
    y = df[target]
    return train_test_split(X, y, test_size=test_size, random_state=42)

def train_logistic_regression(X_train: pd.DataFrame, y_train: pd.Series) -> LogisticRegression:
    """
    Train a logistic regression model.
    
    Parameters:
    X_train (pd.DataFrame): The features of the training set.
    y_train (pd.Series): The target variable of the training set.
    
    Returns:
    LogisticRegression: The trained logistic regression model.
    """
    model = LogisticRegression()
    model.fit(X_train, y_train)
    return model

def train_svm(X_train: pd.DataFrame, y_train: pd.Series) -> SVC:
    """
    Train a Support Vector Machine (SVM) model.
    
    Parameters:
    X_train (pd.DataFrame): The features of the training set.
    y_train (pd.Series): The target variable of the training set.
    
    Returns:
    SVC: The trained SVM model.
    """
    model = SVC()
    model.fit(X_train, y_train)
    return model

def train_random_forest(X_train: pd.DataFrame, y_train: pd.Series) -> RandomForestClassifier:
    """
    Train a Random Forest model.
    
    Parameters:
    X_train (pd.DataFrame): The features of the training set.
    y_train (pd.Series): The target variable of the training set.
    
    Returns:
    RandomForestClassifier: The trained Random Forest model.
    """
    model = RandomForestClassifier()
    model.fit(X_train, y_train)
    return model

def evaluate_model(model, X_test: pd.DataFrame, y_test: pd.Series) -> None:
    """
    Evaluate the performance of the model.
    
    Parameters:
    model: The trained model.
    X_test (pd.DataFrame): The features of the test set.
    y_test (pd.Series): The target variable of the test set.
    """
    y_pred = model.predict(X_test)
    accuracy = accuracy_score(y_test, y_pred)
    print("Accuracy Score:", accuracy)
    print("Classification Report:")
    print(classification_report(y_test, y_pred))
    print("Confusion Matrix:")
    print(confusion_matrix(y_test, y_pred))

if __name__ == "__main__":
    # Load data
    data = load_data('../data/processed/lung_cancer_processed.csv')

    # Split data
    X_train, X_test, y_train, y_test = split_data(data, 'LUNG_CANCER', test_size=0.2)

    # Train models
    lr_model = train_logistic_regression(X_train, y_train)
    svm_model = train_svm(X_train, y_train)
    rf_model = train_random_forest(X_train, y_train)

    # Evaluate models
    print("Logistic Regression:")
    evaluate_model(lr_model, X_test, y_test)
    print("\nSupport Vector Machine:")
    evaluate_model(svm_model, X_test, y_test)
    print("\nRandom Forest:")
    evaluate_model(rf_model, X_test, y_test)
