import pandas as pd
from sklearn.model_selection import train_test_split

def load_data(filepath: str) -> pd.DataFrame:
    """
    Load the dataset from a CSV file.
    
    Parameters:
    filepath (str): The path to the CSV file.
    
    Returns:
    pd.DataFrame: The loaded dataset.
    """
    return pd.read_csv(filepath)

def split_data(df: pd.DataFrame, target: str, test_size: float) -> (pd.DataFrame, pd.DataFrame, pd.Series, pd.Series):
    """
    Split the dataset into train and test sets.
    
    Parameters:
    df (pd.DataFrame): The dataset.
    target (str): The target column.
    test_size (float): The proportion of the dataset to include in the test split.
    
    Returns:
    (pd.DataFrame, pd.DataFrame, pd.Series, pd.Series): X_train, X_test, y_train, y_test
    """
    X = df.drop(columns=[target])
    y = df[target]
    return train_test_split(X, y, test_size=test_size, random_state=42)
